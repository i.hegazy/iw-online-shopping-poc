import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { CharactersModule } from './characters/characters.module'
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
//import { DashboardModule } from './dashboard/dashboard.module';
//import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
//import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AllocationModule } from './allocation/allocation.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    // DashboardModule,
    // ECommerceModule,
    // MiscellaneousModule,
    CharactersModule,
    AllocationModule,
    SharedModule

  ],
  declarations: [
    PagesComponent
  ],
})
export class PagesModule {
}
