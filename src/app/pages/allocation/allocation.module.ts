import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllocationRoutingModule } from './allocation-routing.module';
import { AllocationComponent } from './allocation.component';


import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbTreeGridModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { CharacterSelectComponent } from './character-select/character-select.component';
import { ItemsFitComponent } from './items-fit/items-fit.component';
import { AllocationSceneComponent } from './allocation-scene/allocation-scene.component';
import { SharedModule } from '../shared/shared.module';
//import { CharacterSceneComponent } from '../characters/character-scene/character-scene.component';
//import { CharacterSceneComponent } from '../character-scene/character-scene.component'

@NgModule({
  declarations: [
    AllocationComponent,
    CharacterSelectComponent,
    ItemsFitComponent,
    AllocationSceneComponent],

  imports: [
    CommonModule,
    AllocationRoutingModule,
    Ng2SmartTableModule,
    NbCardModule, NbTreeGridModule, NbIconModule, NbInputModule,
    ThemeModule,
    SharedModule
  ]
})
export class AllocationModule { }
