import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllocationComponent } from './allocation.component';
import { CharacterSelectComponent } from './character-select/character-select.component';
import { AllocationSceneComponent } from './allocation-scene/allocation-scene.component';
import { ItemsFitComponent } from './items-fit/items-fit.component';
import { CharacterSceneComponent } from '../shared/character-scene/character-scene.component';

const routes: Routes = [
  {
    path: '', component: AllocationComponent, children: [
      // { path: '', component: CharacterSelectComponent },
      { path: 'allocation-scene', component: AllocationSceneComponent },
      { path: 'character-select', component: CharacterSelectComponent },
      { path: 'items-fit', component: ItemsFitComponent },
      { path: 'character-scene', component: CharacterSceneComponent }
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllocationRoutingModule { }
