import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-character-select',
  templateUrl: './character-select.component.html',
  styleUrls: ['./character-select.component.scss']
})
export class CharacterSelectComponent implements OnInit {

  settings = {
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   createButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    // },
    hideSubHeader: true,
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      selected: {
        title: 'Selected Character',
        type: 'html'
      },
      id: {
        title: 'Character ID',
        type: 'number',
      },
      name: {
        title: 'Character Name',
        type: 'string',
      },
      gender: {
        title: 'Character Gender',
        type: 'string',
      },
      date: {
        title: 'Creation Date',
        type: 'date',
      }
    }
  };

  filterValue: string = '';

  getFilterValue(event, type) {
    this.filterValue = event.target.value
    this.source.addFilter({ field: type, search: this.filterValue })
  }
  source: LocalDataSource = new LocalDataSource();
  data = [
    { selected: "<label>hi,i am radio</label><input type='radio'></input>", id: 1, name: 'mahmoud', gender: 'male', date: '4-2-2020' },
    { selected: "<label>hi,i am radio</label><input type='radio'></input>", id: 2, name: 'eslam', gender: 'male', date: '4-2-2020' },
    { selected: "<label>hi,i am radio</label><input type='radio'></input>", id: 2, name: 'fatma', gender: 'female', date: '4-2-2020' },
    { selected: "<label>hi,i am radio</label><input type='radio'></input>", id: 4, name: 'doaa', gender: 'female', date: '28-1-2020' }
  ]


  constructor() {
    this.source.load(this.data)

  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log("delete event", event)
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    console.log("edit event ", event)
    if (window.confirm('Are you sure you want to edit?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }


}
