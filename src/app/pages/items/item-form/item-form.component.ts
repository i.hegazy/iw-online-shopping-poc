import { ItemsService } from './../items.service';
import { Items } from './../items.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ngx-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnInit {
  selectedItemSubTypes = false;
  ItemSubType = "";
  GenderFit = [
    "Adult Male", "Adult Female", "Adult Uni-sex", "Teen Male", "Teen Female", "Teen Uni-sex", "Children Boy", "Children Girl", "Children Uni-sex"];

  ItemSubTypes = {
    'Adult Male': ['Shirt', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    "Adult Female": ['Dress', 'Blouses', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    "Adult Uni-sex": ['T-Shirt', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    "Teen Male": ['Shirt', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    "Teen Female": ['Dress', 'Blouses', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    "Teen Uni-sex": ['Shirt', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care']
    ,

    'Children Body': ['Shirt', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care'],

    'Children Girl': ['Shirt', 'T-Shirt', 'Jacket', 'Jeans', 'Shoes', 'Bags', 'Leather', 'Edu Toilet', 'Edu Perfume', 'Face Care', 'Body Care']
  }
  constructor(private ItemsService: ItemsService) { }

  ngOnInit() {
  }
  onGenderFit(event) {
    this.ItemSubType = "Choose...";
    this.selectedItemSubTypes = this.ItemSubTypes[event];
    console.log(this.selectedItemSubTypes);

  }
  onSubmit(f: NgForm) {
    const value = f.value;
    const newItem = new Items(
      [],
      [],
      [],
      [],
      value.sn,
      value.name,
      value.genderType,
      value.genderFit,
      value.type,
      value.subtype1,
      null,
      value.cutStyle,
      value.color,
      value.brand,
      value.sizeUnit,
      value.size,
      value.shoulderToFeet,
      value.shoulderToHeel,
      value.shoulderToHip,
      value.chest,
      value.waist,
      value.neck,
      value.leg,
      value.sleeve
    );
    this.ItemsService.addItem(newItem).subscribe(res => {
      console.log(res)
      f.reset();
    });
  }
}
