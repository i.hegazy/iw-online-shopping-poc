import { Router, ActivatedRoute } from '@angular/router';
import { Items } from './../items.model';
import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table'
import { ItemsService } from '../items.service';

@Component({
  selector: 'ngx-item-filter',
  templateUrl: './item-filter.component.html',
  styleUrls: ['./item-filter.component.scss']
})
export class ItemFilterComponent implements OnInit {
  itemId: number;
  settings = {
    hideSubHeader: true,
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      edit: false,
      delete: true,
      custom: [
        { name: 'edit', title: '<i class="nb-edit"></i>' }
      ]
    },
    columns: {
      id: {
        title: 'Item ID',
        type: 'number',
      },
      name: {
        title: 'Item Name',
        type: 'string',
      },
      type: {
        title: 'Item Category',
        type: 'string',
      },
      registerationDate: {
        title: 'Creation Date',
        type: 'string',
      },
      subtype1: {
        title: 'Item Type',
        type: 'string',
      },
      subtype: {
        title: 'Item Sub-Type',
        type: 'string',
      },
      genderType: {
        title: 'Gender Type',
        type: 'string',
      },
      genderFit: {
        title: 'Gender Fit',
        type: 'string',
      },
      size: {
        title: 'Item Size',
        type: 'string',
      },
      color: {
        title: 'Item Color',
        type: 'string',
      },
      // season: {
      //   title: 'Item Season',
      //   type: 'string',
      // }
    }
  };

  filterValue: string = '';

  getFilterValue(event, type) {
    this.filterValue = event.target.value
    this.source.addFilter({ field: type, search: this.filterValue })
  }

  source: LocalDataSource = new LocalDataSource();
  data = []
  constructor(private ItemsService: ItemsService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.ItemsService.geAllitems().subscribe((res: Items[]) => {
      this.data = res;
      this.source.load(this.data);
      console.log(res)
    })
  }

  onDeleteConfirm(event): void {
    //console.log("delete event", event)
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.ItemsService.deleteitem(event.data.id).subscribe((res) => {
        //console.log(res)
      })
    } else {
      event.confirm.reject();
    }
  }

  onCustomAction(event) {
    switch (event.action) {
      case 'edit':
        this.edit(event.data);
    }
  }
  public edit(formData: any) {
    console.log(formData.id)
    this.itemId = formData.id;
    this.router.navigate(['../item-edit', this.itemId], { relativeTo: this.route });
  }

}
