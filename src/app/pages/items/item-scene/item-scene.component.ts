import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ngx-item-scene',
  templateUrl: './item-scene.component.html',
  styleUrls: ['./item-scene.component.scss']
})
export class ItemSceneComponent implements OnInit {
  @ViewChild('canvas', { static: true }) private canvasRef: ElementRef
  constructor() { }

  ngOnInit() {
    var c = this.canvas
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.arc(95, 50, 40, 0, 2 * Math.PI);
    ctx.stroke();
  }
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement
  }
}
