import { Items } from './items.model';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class ItemsService {

    private apiUrl = 'http://192.168.1.200:6060/items/';

    constructor(private http: HttpClient) { }


    geAllitems(): Observable<Items[]> {
        return this.http.get<Items[]>(this.apiUrl + 'all')
    }

    deleteitem(id: number): Observable<Items[]> {
        return this.http.delete<Items[]>(this.apiUrl + 'delete/' + id)
    }

    addItem(item: Items): Observable<Items[]> {
        return this.http.post<Items[]>(this.apiUrl + 'add', item)
    }
}