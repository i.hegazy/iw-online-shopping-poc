import { NgModule } from '@angular/core';


import { CharactersRoutingModule } from './characters-routing.module';
import { CharactersComponent } from './characters.component';
import { CharacterFilterComponent } from './character-filter/character-filter.component';
import { CharacterFormComponent } from './character-form/character-form.component';
import { CharacterCreateComponent } from './character-create/character-create.component';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbTreeGridModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [

    CharactersComponent,
    CharacterFilterComponent,
    CharacterFormComponent,
    //CharacterSceneComponent,
    CharacterCreateComponent
  ],
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    CharactersRoutingModule,
    Ng2SmartTableModule,
    SharedModule

  ]
})
export class CharactersModule { }
