import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterFilterComponent } from './character-filter/character-filter.component';
import { CharactersComponent } from './characters.component';
import { CharacterCreateComponent } from './character-create/character-create.component';
import { CharacterFormComponent } from './character-form/character-form.component';
import { CharacterSceneComponent } from '../shared/character-scene/character-scene.component';

const routes: Routes = [
  {
    path: '', component: CharactersComponent,
    children: [
      // { path: '', redirectTo: 'character-filter' },
      { path: 'character-filter', component: CharacterFilterComponent },
      { path: 'character-create', component: CharacterCreateComponent },
      { path: 'character-form', component: CharacterFormComponent },
      { path: 'character-scene', component: CharacterSceneComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharactersRoutingModule { }
