import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ngx-character-scene',
  templateUrl: './character-scene.component.html',
  styleUrls: ['./character-scene.component.scss']
})
export class CharacterSceneComponent implements OnInit {
  @ViewChild('canvas', { static: true }) private canvasRef: ElementRef
  constructor() { }

  ngOnInit() {
    var c = this.canvas
    var ctx = c.getContext("2d");
    ctx.moveTo(0, 0);
    ctx.lineTo(200, 100);
    ctx.stroke();
  }
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement
  }

}
