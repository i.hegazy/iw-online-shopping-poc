import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterSceneComponent } from './character-scene/character-scene.component'

@NgModule({
  declarations: [CharacterSceneComponent],
  imports: [
    CommonModule,
  ],
  exports: [CharacterSceneComponent]
})
export class SharedModule { }
